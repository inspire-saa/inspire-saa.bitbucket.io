'use strict';

// add click listener to all elements with countyclick class
// call showSAA and pass in element on click
document.querySelectorAll('.countyclick').forEach(county => county.addEventListener('click', function() {
    showSAA(county);
}));

document.querySelectorAll('.toggleCountyDisplay').forEach(display => display.addEventListener('click', function() {

    this.nextElementSibling.classList.toggle("mapDisplay");
    this.nextElementSibling.classList.toggle("gridDisplay");

    if (this.nextElementSibling.id === "map") {
        document.getElementById("grid").classList.toggle("mapDisplay");
        document.getElementById("grid").classList.toggle("gridDisplay");
    } else if (this.nextElementSibling.id === "grid") {
        document.getElementById("map").classList.toggle("mapDisplay");
        document.getElementById("map").classList.toggle("gridDisplay");
    }

}));

// called when county is clicked
function showSAA(county) {

    // hide prompt
    document.getElementById('saa-top').style.display = "none";

    // pass in county id to list teachers
    listTeachers(county.getAttribute('id'));
}

function listTeachers(county) {
    var hst_list = document.getElementById('hst-list');
    var acc = document.getElementsByClassName("acc-teachers");
    var list_html = ""; // html to be injected later

    // dict of SAAs with contact info
    const saas = {
        'Alicia Avalos': {
            phone: '(626) 317-0112 ext. 1145',
            email: 'aliciaa@inspireschools.org'},
        'Valerie Barrera': {
            phone: '(626) 317-0112 ext. 1053',
            email: 'valerie@inspireschools.org'},
        'Kyle Calvert': {
            phone: '(626) 317-0112 ext. 1050',
            email: 'kcalvert@inspireschools.org'},
        'An Cazares': {
            phone: '(626) 317-0112 ext. 1058',
            email: 'an.cazares@inspireschools.org'},
        'Veronica Delgado': {
            phone: '(626) 317-0112 ext. 1141',
            email: 'vdelgado@inspireschools.org'},
        'Jocelynn Flores': {
            phone: '(626) 317-0112 ext. xxxx',
            email: 'jocelynn.flores@inspireschools.org'},
        'Carlos Garcia': {
            phone: '(626) 317-0112 ext. 1530',
            email: 'carlos.garcia@inspireschools.org'},
        'Josue Garcia': {
            phone: '(626) 317-0112 ext. 1130',
            email: 'josueg@inspireschools.org'},
        'Kristopher Guerrero': {
            phone: '(626) 317-0112 ext. 1528',
            email: 'kristopher.guerrero@inspireschools.org'},
        'Cathleen Hernandez': {
            phone: '(626) 317-0112 ext. 1531',
            email: 'cathleen.hernandez@inspireschools.org'},
        'Alan Jimenez': {
            phone: '(626) 317-0112 ext. 1226',
            email: 'alan@inspireschools.org'},
        'Rene Lizcano': {
            phone: '(626) 317-0112 ext. 1529',
            email: 'rene@inspireschools.org'},
        'Monica Montes': {
            phone: '(626) 317-0112 ext. 1150',
            email: 'monicam@inspireschools.org'},
        'Roberto Perez': {
            phone: '(626) 317-0112 ext. 1500',
            email: 'roberto.perez@inspireschools.org'},
        'Steven Rubalcava': {
            phone: '(626) 317-0112 ext. 1152',
            email: 'stevenr@inspireschools.org'},
        'Cindy Salmeron': {
            phone: '(626) 317-0112 ext. 1164',
            email: 'cindy@inspireschools.org'},
        'Carolyn Shipman': {
            phone: '(626) 317-0112 ext. 1113',
            email: 'caroline@inspireschools.org'},
        'Maria Singh': {
            phone: '(626) 317-0112 ext. 1043',
            email: 'msingh@inspireschools.org'},
        'Karen So': {
            phone: '(626) 317-0112 ext. 1494',
            email: 'karen.so@inspireschools.org'},
        'Jenie Soriano-Francoso': {
            phone: '(626) 317-0112 ext. 1175',
            email: 'jenie@inspireschools.org'},
        'Joshua Sparks': {
            phone: '',
            email: 'joshuas@inspireschools.org'},
        'Cynthia Valenzuela': {
            phone: '(626) 317-0112 ext. 1171',
            email: 'cynthiav@inspireschools.org'}
    };

    // dict of RCs and their SAA
    const teachers = {
        'Susan Byers' : 'Valerie Barrera',
        'Megan Miller' : 'Valerie Barrera',
        'Laura Armbruster' : 'Valerie Barrera',
        'Elaine Rodriguez' : 'Valerie Barrera',
        'Teresa Budke' : 'Roberto Perez',
        'Tiffney DeNuccio' : 'Roberto Perez',
        'Julie Leroux' : 'Roberto Perez',
        'Lisa Salazar' : 'Roberto Perez',
        'Sofiya Sverdlova-Turin' : 'Roberto Perez',
        'Rachael Sullivan' : 'Kyle Calvert',
        'Heidi Hernandez' : 'Kyle Calvert',
        'Shelly Pocinich' : 'Kyle Calvert',
        'Nicole Burwell' : 'Kyle Calvert',
        'Carole Shaw' : 'Kyle Calvert',
        'Monica Shakin' : 'Cynthia Valenzuela',
        'Erin Kyle' : 'Cynthia Valenzuela',
        'Desiree Ramirez' : 'Cynthia Valenzuela',
        'Katie Lucas' : 'Cynthia Valenzuela',
        'Hollie Smith' : 'Alan Jimenez',
        'Samantha Haynes' : 'Alan Jimenez',
        'Emily Cruz' : 'Alan Jimenez',
        'Elizabeth Embick' : 'Alan Jimenez',
        'Dawn Gordon' : 'Alan Jimenez',
        'Sara Newcomb' : 'Alan Jimenez',
        'Laura Rankin' : 'Alan Jimenez',
        'Courtney McCorkle' : 'Alan Jimenez',
        'Christine Ang' : 'Alan Jimenez',
        'Callie Johnson' : 'Alan Jimenez',
        'Sarah Elizabeth Elliot' : 'Alan Jimenez',
        'Marnee Reeves' : 'Alan Jimenez',
        'Jeanette Redstone' : 'Alan Jimenez',
        'Carin Beard' : 'Alan Jimenez',
        'Nancy Major' : 'Alan Jimenez',
        'Kacy Coalwell' : 'Alan Jimenez',
        'Diana Billings' : 'Cathleen Hernandez',
        'Kate Rowe' : 'Cathleen Hernandez',
        'Natalie Barker' : 'Cathleen Hernandez',
        'Carly Boyd-Dovideo' : 'Cathleen Hernandez',
        'Ashley Warmuth' : 'Cathleen Hernandez',
        'Charity Tebbs-Rovelo' : 'Cathleen Hernandez',
        'Monique Verdin' : 'Cathleen Hernandez',
        'Jill Schalesky' : 'Cathleen Hernandez',
        'Erin Richardson' : 'Veronica Delgado',
        'Christie Chambless' : 'Veronica Delgado',
        'Nicole Gallella' : 'Veronica Delgado',
        'Bethany Rubino' : 'Veronica Delgado',
        'Sharon Joseph' : 'Veronica Delgado',
        'Rachel Embry' : 'Veronica Delgado',
        'Amy Davis' : 'Carlos Garcia',
        'Hope Cutter' : 'Carlos Garcia',
        'Mary Collier' : 'Carlos Garcia',
        'Katie Cunningham' : 'Carlos Garcia',
        'Brandi Neal' : 'Carlos Garcia',
        'Zena Fisher' : 'Carlos Garcia',
        'Sara Coronado' : 'Maria Singh',
        'Kari Arnson' : 'Maria Singh',
        'Caitlin Martin' : 'Maria Singh',
        'Kathryn Bock' : 'Maria Singh',
        'Courtney Bostock' : 'Jenie Soriano-Francoso',
        'Jennifer Rushing' : 'Jenie Soriano-Francoso',
        'Deborah Holm' : 'Jenie Soriano-Francoso',
        'Erin Dudley-Krizek' : 'Jenie Soriano-Francoso',
        'Jamie Corcoran' : 'Jenie Soriano-Francoso',
        'Jennifer Lorge' : 'Jenie Soriano-Francoso',
        'Christy Burke' : 'Jenie Soriano-Francoso',
        'Sherry Johnson' : 'Jenie Soriano-Francoso',
        'Tracy Cecola' : 'Jenie Soriano-Francoso',
        'Lori Silvio' : 'Jenie Soriano-Francoso',
        'Lisa Watson' : 'Jocelynn Flores',
        'Erin Mike' : 'Jocelynn Flores',
        'Anna Eshleman' : 'Jocelynn Flores',
        'Shannon Giese' : 'Jocelynn Flores',
        'Cori Bero' : 'Jocelynn Flores',
        'Natalie Crouse' : 'Jocelynn Flores',
        'Jamie Jardim' : 'Jocelynn Flores',
        'Linda Barr' : 'Jocelynn Flores',
        'Connie Peacock' : 'Rene Lizcano',
        'Sunny Schweers' : 'Rene Lizcano',
        'Deanna Zamiska' : 'Rene Lizcano',
        'Kristen Trudell' : 'Rene Lizcano',
        'Linsey Hayashi' : 'Rene Lizcano',
        'Kimberly Phillips' : 'Rene Lizcano',
        'Jennifer Carrete' : 'Rene Lizcano',
        'Monica Martin' : 'Rene Lizcano',
        'Krysin Demofonte' : 'Carolyn Shipman',
        'Jessica Bischalaney' : 'Carolyn Shipman',
        'Kristen Burer' : 'Carolyn Shipman',
        'Vanessa Chase' : 'Carolyn Shipman',
        'Erica Corioso' : 'Carolyn Shipman',
        'Michelle Sheridan' : 'Carolyn Shipman',
        'Marlena Wood' : 'Carolyn Shipman',
        'Lacey Rose' : 'Carolyn Shipman',
        'Julie Contreras' : 'Josue Garcia',
        'Candice Betz' : 'Josue Garcia',
        'Diane Dunning' : 'Josue Garcia',
        'Heather Shellhammer' : 'Josue Garcia',
        'Kimberly Eliason' : 'Cindy Salmeron',
        'Ashley Benjamin' : 'Monica Montes',
        'Charlotte Zavadil' : 'Monica Montes',
        'Brook MacMillan' : 'An Cazares',
        'Deborah Cruthers' : 'An Cazares',
        'Jennie Fazzio' : 'An Cazares',
        'Ana Mejia' : 'An Cazares',
        'Kristy Philips' : 'An Cazares',
        'Jessica Ronquillo' : 'An Cazares',
        'Jennifer Sweet' : 'An Cazares',
        'Betsy West' : 'An Cazares',
        'Lauren Anderson' : 'An Cazares',
        'Pamela Bezemer' : 'An Cazares',
        'Stephanie Johnson' : 'An Cazares',
        'Stephanie Williams' : 'An Cazares',
        'Natalie Douty' : 'An Cazares',
        'Bethany Brewer' : 'Kristopher Guerrero',
        'Julie Roszkowicz' : 'Kristopher Guerrero',
        'Stephanie Cronshaw' : 'Kristopher Guerrero',
        'Christina Philips' : 'Kristopher Guerrero',
        'Alison Golden' : 'Kristopher Guerrero',
        'Jaime Grasmick' : 'Kristopher Guerrero',
        'Holly Dees' : 'Steven Rubalcava',
        'Heather Stokhaug' : 'Steven Rubalcava',
        'Marci Boyd' : 'Steven Rubalcava',
        'Elisa Avila' : 'Steven Rubalcava',
        'Carrie Carlson' : 'Steven Rubalcava',
        'Scott Emerson' : 'Joshua Sparks',
        'Tyrone Beekman' : 'Joshua Sparks',
        'Amber Stephen' : 'Joshua Sparks',
        'Teresa Brown' : 'Joshua Sparks',
        'Deanna Dyer' : 'Joshua Sparks',
        'Camille Vocker' : 'Joshua Sparks',
        'Jenell Sherman' : 'Alicia Avalos',
        'Shannon Breckenridge' : 'Alicia Avalos',
        'Kirsten Graat' : 'Alicia Avalos',
        'Kristie Nicosia' : 'Alicia Avalos',
        'Allison Suydam' : 'Alicia Avalos',
        'Ronni Ernenputsch' : 'Alicia Avalos',
        'Kerstin Mentink' : 'Alicia Avalos',
        'Cindy Huisking' : 'Alicia Avalos',
        'Jennifer Johnston' : 'Alicia Avalos',
        'Kara Tupy' : 'Alicia Avalos',
        'Melanie Hemaidan' : 'Karen So',
        'Carrie Stumpfhauser' : 'Karen So',
        'Kimberly Souder' : 'Karen So',
        'Mary Lowe' : 'Karen So',
        'Linda Empleo' : 'Karen So',
        'Maria Thoeni' : 'Karen So'
    };

    // dict of counties with RCs
    const counties = {
        'Alameda': [],
        'Alpine': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees',  'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Amador': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Butte': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Calaveras': [],
        'Colusa': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Contra_Costa': [],
        'Del_Norte_1_': [],
        'El_Dorado': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'], 
        'Fresno': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Glenn': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Humboldt': [],
        'Imperial': ['Ashley Benjamin', 'Candice Betz', 'Diane Dunning', 'Heather Shellhammer', 'Julie Contreras', 'Kimberly Eliason', 'Lacey Rose', 'Marlena Wood', 'Michelle Sheridan', 'Monica Martin'],
        'Inyo': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Kern': ['Ashley Warmuth', 'Bethany Rubino', 'Carin Beard', 'Carly Boyd-Dovideo', 'Charity Tebbs-Rovelo', 'Christie Chambless', 'Erin Richardson', 'Jeanette Redstone', 'Jill Schalesky', 'Kacy Coalwell', 'Marnee Reeves', 'Monique Verdin', 'Nancy Major', 'Natalie Barker', 'Nicole Gallella', 'Diana Billings', 'Rachel Embry', 'Sarah Elizabeth Elliot', 'Sharon Joseph'],
        'Kings': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Lake': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Lassen': [],
        'Los_Angeles': ['Carole Shaw', 'Desiree Ramirez', 'Elaine Rodriguez', 'Erin Kyle', 'Heidi Hernandez', 'Julie Leroux', 'Katie Lucas', 'Laura Armbruster', 'Lisa Salazar', 'Megan Miller', 'Monica Shakin', 'Nicole Burwell', 'Rachael Sullivan', 'Shelly Pocinich', 'Sofiya Sverdlova-Turin', 'Susan Byers', 'Teresa Budke', 'Tiffney DeNuccio'],
        'Madera': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Marin': [],
        'Mariposa': [],
        'Mendocino': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Merced': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Modoc': [],
        'Mono': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Monterey': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Napa': [],
        'Nevada': [],
        'Orange': ['Anna Eshleman', 'Cori Bero', 'Erin Mike', 'Jamie Jardim', 'Jennifer Carrete', 'Kimberly Phillips', 'Kristen Trudell', 'Linda Barr', 'Linsey Hayashi', 'Lisa Watson', 'Lori Silvio', 'Natalie Crouse', 'Shannon Giese', 'Tracy Cecola'],
        'Placer': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees',  'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Plumas': [],
        'Riverside': ['Brandi Neal', 'Caitlin Martin', 'Deborah Holm', 'Erin Dudley-Krizek', 'Jamie Corcoran', 'Kari Arnson', 'Kathryn Bock', 'Katie Cunningham', 'Mary Collier', 'Sara Coronado', 'Zena Fisher'],
        'Sacramento': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'San_Benito': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'San_Bernardino': ['Alison Golden', 'Bethany Brewer', 'Betsy West', 'Brook MacMillan', 'Christina Philips', 'Jaime Grasmick', 'Jennifer Sweet', 'Julie Roszkowicz', 'Lauren Anderson', 'Stephanie Cronshaw'], 
        'San_Diego': ['Ashley Benjamin', 'Candice Betz', 'Diane Dunning', 'Heather Shellhammer', 'Julie Contreras', 'Kimberly Eliason', 'Lacey Rose', 'Marlena Wood', 'Michelle Sheridan', 'Monica Martin'],
        'San_Francisco': [],
        'San_Joaquin': [],
        'San_Luis_Obispo': ['Carole Shaw', 'Desiree Ramirez', 'Elaine Rodriguez', 'Erin Kyle', 'Heidi Hernandez', 'Julie Leroux', 'Katie Lucas', 'Laura Armbruster', 'Lisa Salazar', 'Megan Miller', 'Monica Shakin', 'Nicole Burwell', 'Rachael Sullivan', 'Shelly Pocinich', 'Sofiya Sverdlova-Turin', 'Susan Byers', 'Teresa Budke', 'Tiffney DeNuccio'],
        'San_Mateo': [],
        'Santa_Barbara': ['Carole Shaw', 'Desiree Ramirez', 'Elaine Rodriguez', 'Erin Kyle', 'Heidi Hernandez', 'Julie Leroux', 'Katie Lucas', 'Laura Armbruster', 'Lisa Salazar', 'Megan Miller', 'Monica Shakin', 'Nicole Burwell', 'Rachael Sullivan', 'Shelly Pocinich', 'Sofiya Sverdlova-Turin', 'Susan Byers', 'Teresa Budke', 'Tiffney DeNuccio'],
        'Santa_Clara': [],
        'Santa_Cruz': [],
        'Shasta': [],
        'Sierra': [],
        'Siskiyou': [],
        'Solano': [],
        'Sonoma': [],
        'Stanislaus': [],
        'Sutter': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Tehama': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Trinity': [],
        'Tulare': ['Carrie Stumpfhauser', 'Kimberly Souder', 'Linda Empleo', 'Maria Thoeni', 'Mary Lowe', 'Melanie Hemaidan', 'Natalie Douty', 'Stephanie Williams'],
        'Tuolumne': [],
        'Ventura': ['Carole Shaw', 'Desiree Ramirez', 'Elaine Rodriguez', 'Erin Kyle', 'Heidi Hernandez', 'Julie Leroux', 'Katie Lucas', 'Laura Armbruster', 'Lisa Salazar', 'Megan Miller', 'Monica Shakin', 'Nicole Burwell', 'Rachael Sullivan', 'Shelly Pocinich', 'Sofiya Sverdlova-Turin', 'Susan Byers', 'Teresa Budke', 'Tiffney DeNuccio'],
        'Yolo': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees', 'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman'],
        'Yuba': ['Amber Stephen', 'Camille Vocker', 'Carrie Carlson', 'Cindy Huisking', 'Deanna Dyer', 'Elisa Avila', 'Heather Stokhaug', 'Holly Dees',  'Jennifer Johnston', 'Kara Tupy', 'Kerstin Mentink', 'Marci Boyd', 'Ronni Ernenputsch', 'Scott Emerson', 'Teresa Brown', 'Tyrone Beekman']
    };

    // use county to pick correct teacher list
    var teacher_list = counties[county];
    var county_header = county.replace('_1_', '').replace(/_+/g, ' ');
    list_html += '<h2>' + county_header + '</h2>';
    //list_html += document.getElementById(county).getAttribute('data-original-title');
    if (teacher_list.length > 0) {
        list_html += '<p>Find your RC below:</p>';
        for (var teacher in teacher_list) {
            // create accordian using teachers found for county
            list_html += '<button class="accordion acc-teachers">' + teacher_list[teacher] + '</button><div class="acc-panel"><div><p>saa: '+ teachers[teacher_list[teacher]] + '</p></div><p>phone: '+ saas[teachers[teacher_list[teacher]]].phone + '</p><p>email: '+ saas[teachers[teacher_list[teacher]]].email + '</p></div>';
            console.log(teacher_list[teacher]);
        }
    } else {
        list_html += '<p>No teachers found in this county.</p>';
    }

    // add html to #html-list and make it visible
    hst_list.innerHTML = list_html;
    hst_list.style.display = "block";

    for (var i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            /* Toggle between adding and removing the "active" class,
            to highlight the button that controls the panel */
            this.classList.toggle("active");

            /* Toggle between hiding and showing the active panel */
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }

    hst_list.scrollIntoView({behavior: "smooth"});
}
